#
# Automatically generated file. DO NOT MODIFY
#

AB_OTA_PARTITIONS += \
    LOGO \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    hyp \
    imagefv \
    keymaster \
    modem \
    multiimgoem \
    qupfw \
    storsec \
    tz \
    uefisecapp \
    xbl \
    xbl_config
